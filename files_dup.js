var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", "BNO055_8py" ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop.ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", null ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "ME305_Lab0.py", "ME305__Lab0_8py.html", null ],
    [ "ME305_Lab1_Pang_Wimberley.py", "ME305__Lab1__Pang__Wimberley_8py.html", "ME305__Lab1__Pang__Wimberley_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.ControllerTasks", "classtask__controller_1_1ControllerTasks.html", "classtask__controller_1_1ControllerTasks" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.EncoderTasks", "classtask__encoder_1_1EncoderTasks.html", "classtask__encoder_1_1EncoderTasks" ]
    ] ],
    [ "task_imu.py", "task__imu_8py.html", [
      [ "task_imu.IMUTasks", "classtask__imu_1_1IMUTasks.html", "classtask__imu_1_1IMUTasks" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.MotorTasks", "classtask__motor_1_1MotorTasks.html", "classtask__motor_1_1MotorTasks" ]
    ] ],
    [ "task_safety.py", "task__safety_8py.html", [
      [ "task_safety.SafetyTasks", "classtask__safety_1_1SafetyTasks.html", "classtask__safety_1_1SafetyTasks" ]
    ] ],
    [ "task_touch.py", "task__touch_8py.html", [
      [ "task_touch.TouchTasks", "classtask__touch_1_1TouchTasks.html", "classtask__touch_1_1TouchTasks" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.UserTasks", "classtask__user_1_1UserTasks.html", "classtask__user_1_1UserTasks" ]
    ] ],
    [ "Touch_Panel.py", "Touch__Panel_8py.html", "Touch__Panel_8py" ]
];