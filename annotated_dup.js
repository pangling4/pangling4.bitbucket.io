var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "ControllerTasks", "classtask__controller_1_1ControllerTasks.html", "classtask__controller_1_1ControllerTasks" ]
    ] ],
    [ "task_encoder", null, [
      [ "EncoderTasks", "classtask__encoder_1_1EncoderTasks.html", "classtask__encoder_1_1EncoderTasks" ]
    ] ],
    [ "task_imu", null, [
      [ "IMUTasks", "classtask__imu_1_1IMUTasks.html", "classtask__imu_1_1IMUTasks" ]
    ] ],
    [ "task_motor", null, [
      [ "MotorTasks", "classtask__motor_1_1MotorTasks.html", "classtask__motor_1_1MotorTasks" ]
    ] ],
    [ "task_safety", null, [
      [ "SafetyTasks", "classtask__safety_1_1SafetyTasks.html", "classtask__safety_1_1SafetyTasks" ]
    ] ],
    [ "task_touch", null, [
      [ "TouchTasks", "classtask__touch_1_1TouchTasks.html", "classtask__touch_1_1TouchTasks" ]
    ] ],
    [ "task_user", null, [
      [ "UserTasks", "classtask__user_1_1UserTasks.html", "classtask__user_1_1UserTasks" ]
    ] ],
    [ "Touch_Panel", null, [
      [ "Touch_Panel", "classTouch__Panel_1_1Touch__Panel.html", "classTouch__Panel_1_1Touch__Panel" ]
    ] ]
];