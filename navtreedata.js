/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ball Balancing Platform", "index.html", [
    [ "Mechatronics Repository", "index.html", null ],
    [ "Files", "Intro.html", [
      [ "Introduction", "Intro.html#sec_intro", null ],
      [ "Labs", "Intro.html#sec_main", null ],
      [ "Lab Descriptions:", "Intro.html#sec_lab", null ]
    ] ],
    [ "3 HW0x02", "page.html", [
      [ "Ball Balancer System Modeling", "page.html#HW0x23", null ],
      [ "Simulink  Simulation", "page.html#Matlab", null ]
    ] ],
    [ "Lab0", "Lab0.html", [
      [ "Lab0 Fibonacci", "Lab0.html#sec_lab0", null ]
    ] ],
    [ "Lab1", "Lab1.html", [
      [ "Lab 1 LED", "Lab1.html#sec_lab1", null ]
    ] ],
    [ "Lab2", "Lab2.html", [
      [ "Lab 2 Encoder Driver", "Lab2.html#sec_lab2", null ]
    ] ],
    [ "Lab3", "Lab3.html", [
      [ "Lab 3 Motor Driver and User Interface", "Lab3.html#sec_lab3", null ]
    ] ],
    [ "Lab4", "Lab4.html", [
      [ "Lab 4 Closed Loop Controller", "Lab4.html#sec_lab4", null ],
      [ "Results    Test Results", "Lab4.html#ControlLoop", null ]
    ] ],
    [ "Lab5", "Lab5.html", [
      [ "Lab 5 Inertial Measurement Unit Driver", "Lab5.html#sec_lab5", null ]
    ] ],
    [ "Term_Project", "Term_Project.html", [
      [ "Deliverables", "Term_Project.html#Final", null ],
      [ "Touch Panel Test Results", "Term_Project.html#Touch_Panel", null ],
      [ "System Performance Video Demonstration", "Term_Project.html#Ball_Balancing_Demo", null ],
      [ "Term Project Task and State Diagrams", "Term_Project.html#FSM_Diagrams", null ],
      [ "Balancing Data Plots", "Term_Project.html#TP_Plots", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BNO055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';